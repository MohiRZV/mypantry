package com.mohi.mypantry

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.repository.PantryDatabase
import com.mohi.mypantry.repository.productpersistence.ProductDao
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 *   created by Mohi on 9/11/2021
 */
@RunWith(AndroidJUnit4::class)
class ProductDatabaseInstrumentedTest {
    private lateinit var productDao: ProductDao
    private lateinit var db: PantryDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, PantryDatabase::class.java).build()
        productDao = db.productDao()
    }

    @Test
    fun testFindRecipeByProduct() = runBlocking {
        val product = Product("name",444.44)
        productDao.saveProduct(product)
        val byName = productDao.findProductByName("name")
        assertThat(byName[0].quantity, equalTo(product.quantity))
    }

    @Test
    @Throws(Exception::class)
    fun testAddProduct_findByName() = runBlocking{
        val product = Product("water",12.8)
        productDao.saveProduct(product)
        val byName = productDao.findProductByName("water")
        assertThat(byName[0].quantity, equalTo(product.quantity))
    }

    @Test
    fun testAddProduct_findById() = runBlocking {
        val product = Product("water",12.8)
        val id = productDao.saveProduct(product)
        val byID = productDao.findProduct(id)
        assertThat(byID.quantity, equalTo(product.quantity))
    }

    @Test
    fun testUpdateProduct() = runBlocking {
        val product = Product("sugar",84.45)
        val id = productDao.saveProduct(product)
        val newQuantity = 7.77
        product.quantity = newQuantity
        product.productID = id
        productDao.updateProduct(product)
        val byID = productDao.findProduct(id)
        assertThat(byID.quantity, equalTo(newQuantity))
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }
}