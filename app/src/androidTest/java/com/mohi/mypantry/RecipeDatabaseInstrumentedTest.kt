package com.mohi.mypantry

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import com.mohi.mypantry.repository.PantryDatabase
import com.mohi.mypantry.repository.recipepersistence.RecipeDao
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 *   created by Mohi on 9/11/2021
 */
@RunWith(AndroidJUnit4::class)
class RecipeDatabaseInstrumentedTest {
    private lateinit var recipeDao: RecipeDao
    private lateinit var db: PantryDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, PantryDatabase::class.java).build()
        recipeDao = db.recipeDao()
    }

    @Test
    @Throws(Exception::class)
    fun writeUserAndReadInList() = runBlocking{
        val recipe = Recipe("lemonade", "water, lemons and sugar")
        val water = Product("water", 1.0)
        val lemonJuice = Product("lemon juice", 2.0)
        val sugar = Product("sugar",3.4)
        val recipeWithProducts = RecipeWithProducts(recipe, listOf(water, lemonJuice, sugar))
        recipeDao.saveRecipeWithProducts(recipeWithProducts)
        val byProduct = recipeDao.findRecipesByProduct("water")
        assertThat(byProduct[0].recipe.description, equalTo(recipe.description))
    }

    @Test
    fun testSaveRecipeWithProducts() = runBlocking {
        val recipeDescription = "description"
        val recipeName = "myRecipe"
        val recipe = Recipe(recipeName, recipeDescription)
        val ingredients = listOf<Product>(
            Product("apple",3.0),
            Product("sugar",60.5),
            Product("salt",3.0),
            Product("flour",300.0)
        )
        val recipeWithProducts = RecipeWithProducts(recipe, ingredients)
        val id = recipeDao.saveRecipeWithProducts(recipeWithProducts)
        val foundRecipe = recipeDao.getRecipe(id)
        assertThat(foundRecipe.description, equalTo(recipeDescription))
        assertThat(foundRecipe.name, equalTo(recipeName))

        val foundIngredients = recipeDao.getIngredients(id)
        assertThat(foundIngredients.size, equalTo(ingredients.size))
        assertThat(foundIngredients[0].name, equalTo(ingredients[0].name))
    }

    @Test
    fun testGetRecipeWithProducts() = runBlocking {
        val recipeDescription = "description"
        val recipeName = "myRecipe"
        val recipe = Recipe(recipeName, recipeDescription)
        val ingredients = listOf<Product>(
            Product("apple",3.0),
            Product("sugar",60.5),
            Product("salt",3.0),
            Product("flour",300.0)
        )
        val recipeWithProducts = RecipeWithProducts(recipe, ingredients)
        val id = recipeDao.saveRecipeWithProducts(recipeWithProducts)

        val foundRecipeWithProducts = recipeDao.getRecipeWithProducts(id)
        assertThat(foundRecipeWithProducts.recipe.name, equalTo(recipe.name))
        assertThat(foundRecipeWithProducts.recipe.description, equalTo(recipe.description))
        assertThat(foundRecipeWithProducts.products.size, equalTo(ingredients.size))
        assertThat(foundRecipeWithProducts.products[1].name, equalTo(ingredients[1].name))
        assertThat(foundRecipeWithProducts.products[1].quantity, equalTo(ingredients[1].quantity))
    }

    @Test
    fun testUpdateRecipeWithProducts() = runBlocking {
        val recipeDescription = "description"
        val recipeName = "myRecipe"
        val recipe = Recipe(recipeName, recipeDescription)
        val ingredients = listOf<Product>(
            Product("apple",3.0),
            Product("sugar",60.5),
            Product("salt",3.0),
            Product("flour",300.0)
        )
        val recipeWithProducts = RecipeWithProducts(recipe, ingredients)
        val id = recipeDao.saveRecipeWithProducts(recipeWithProducts)


        val updatedDescription = "updatedDescription"
        val savedRecipeWithProducts = recipeDao.getRecipeWithProducts(id)
        val savedIngredients = savedRecipeWithProducts.products
        val newAppleQ = 5.0
        val updatedRecipeWithProducts = RecipeWithProducts(
            recipe.copy(description = updatedDescription, recipeID = id),
            listOf(
                savedIngredients[0].copy(quantity = newAppleQ),
                savedIngredients[1],
                savedIngredients[2],
                savedIngredients[3]
            )
        )
        recipeDao.updateRecipeWithProducts(updatedRecipeWithProducts)

        val foundRecipeWithProducts = recipeDao.getRecipeWithProducts(id)
        assertThat(foundRecipeWithProducts.recipe.name, equalTo(recipe.name))
        assertThat(foundRecipeWithProducts.recipe.description, equalTo(updatedDescription))
        assertThat(foundRecipeWithProducts.products.size, equalTo(ingredients.size))
        assertThat(foundRecipeWithProducts.products[0].name, equalTo(ingredients[0].name))
        assertThat(foundRecipeWithProducts.products[0].quantity, equalTo(newAppleQ))
    }


    //TODO test for update recipe with products when ingredient is removed
    @Test
    fun testUpdateRecipeWithProducts_removeIngredient() = runBlocking {
        val recipeDescription = "description"
        val recipeName = "myRecipe"
        val recipe = Recipe(recipeName, recipeDescription)
        val ingredients = listOf<Product>(
            Product("apple",3.0),
            Product("sugar",60.5),
            Product("salt",3.0),
            Product("flour",300.0)
        )
        val recipeWithProducts = RecipeWithProducts(recipe, ingredients)
        val id = recipeDao.saveRecipeWithProducts(recipeWithProducts)


        val updatedDescription = "updatedDescription"
        val savedRecipeWithProducts = recipeDao.getRecipeWithProducts(id)
        val savedIngredients = savedRecipeWithProducts.products
        val newAppleQ = 75.0
        val updatedIngredients = listOf(
            savedIngredients[0].copy(quantity = newAppleQ),
            savedIngredients[1],
            savedIngredients[2]
        )
        val updatedRecipeWithProducts = RecipeWithProducts(
            recipe.copy(description = updatedDescription, recipeID = id),
            updatedIngredients
        )
        recipeDao.updateRecipeWithProducts(updatedRecipeWithProducts)

        val foundRecipeWithProducts = recipeDao.getRecipeWithProducts(id)
        assertThat(foundRecipeWithProducts.recipe.name, equalTo(recipe.name))
        assertThat(foundRecipeWithProducts.recipe.description, equalTo(updatedDescription))
        assertThat(foundRecipeWithProducts.products.size, equalTo(updatedIngredients.size))
        assertThat(foundRecipeWithProducts.products[0].name, equalTo(ingredients[0].name))
        assertThat(foundRecipeWithProducts.products[0].quantity, equalTo(newAppleQ))
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

}