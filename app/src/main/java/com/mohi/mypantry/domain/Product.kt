package com.mohi.mypantry.domain

import androidx.room.ColumnInfo
import java.io.Serializable
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
created by Mohi on 8/8/2021
 */
@Entity(tableName = "product")
data class Product(
    @ColumnInfo(name = "productName")
    var name: String,
    @ColumnInfo
    var quantity: Double,
    @ColumnInfo
    var imageUriString: String = "",
    @PrimaryKey(autoGenerate = true)
    var productID: Long = 0
    ): Serializable
