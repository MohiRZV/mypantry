package com.mohi.mypantry.domain

import androidx.recyclerview.widget.DiffUtil

/**
 *   created by Mohi on 8/28/2021
 */
class ProductDiffCallback: DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.productID == newItem.productID
    }

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
        return oldItem.name == newItem.name && oldItem.quantity == newItem.quantity
    }
}