package com.mohi.mypantry.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
created by Mohi on 8/8/2021
 */
@Entity(tableName = "recipe")
data class Recipe(
    @ColumnInfo(name = "recipeName")
    var name: String,
    @ColumnInfo
    var description: String,
    @ColumnInfo
    var iconUriString: String = "",
    @PrimaryKey(autoGenerate = true)
    var recipeID: Long = 0
    )
