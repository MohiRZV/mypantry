package com.mohi.mypantry.domain

import androidx.recyclerview.widget.DiffUtil

/**
 *   created by Mohi on 8/30/2021
 */
class RecipeDiffCallback: DiffUtil.ItemCallback<Recipe>() {
    override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
        return oldItem.recipeID == newItem.recipeID
    }

    override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
        return oldItem.description == newItem.description && oldItem.name == newItem.name
    }
}