package com.mohi.mypantry.domain

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 *   created by Mohi on 8/28/2021
 */
@Entity(tableName = "recipe_product", primaryKeys = ["productID", "recipeID"])
data class RecipeProduct(
    val productID: Long,
    val recipeID: Long,
    @ColumnInfo(name = "quantity")
    val neededQuantity: Double
    )
