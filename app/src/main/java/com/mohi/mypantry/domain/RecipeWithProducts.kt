package com.mohi.mypantry.domain

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/28/2021
 */
data class RecipeWithProducts(
    @Embedded val recipe: Recipe,
    @Relation(
        parentColumn = "recipeID",
        entityColumn = "productID",
        associateBy = Junction(RecipeProduct::class)
    )

    val products: List<Product>
)