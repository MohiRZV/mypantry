package com.mohi.mypantry.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeProduct
import com.mohi.mypantry.repository.productpersistence.ProductDao
import com.mohi.mypantry.repository.recipepersistence.RecipeDao

/**
 *   created by Mohi on 8/28/2021
 */
const val DATABASE_NAME = "pantry_database"
const val DATABASE_VERSION = 1
@Database(entities = [Recipe::class, Product::class, RecipeProduct::class], version = DATABASE_VERSION, exportSchema = false)
abstract class PantryDatabase: RoomDatabase() {
    abstract fun recipeDao(): RecipeDao
    abstract fun productDao(): ProductDao
    companion object{
        @Volatile
        private var INSTANCE: PantryDatabase?=null
        fun getDatabase(context: Context): PantryDatabase {
            val tempInstance = INSTANCE
            if(tempInstance!=null)
                return tempInstance

            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PantryDatabase::class.java,
                    DATABASE_NAME
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}