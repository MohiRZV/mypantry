package com.mohi.mypantry.repository.productpersistence

import androidx.room.*
import com.mohi.mypantry.domain.Product
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/24/2021
 */
@Dao
interface ProductDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveProduct(product: Product): Long
    //TODO possible bug? - when the name of the product is updated from the recipe, it doesn't change
    @Update
    fun updateProduct(product: Product)

    @Query("select * from product where productName like :query order by productName asc")
    fun getAllProducts(query: String): Flow<List<Product>>

    @Query("DELETE FROM product")
    fun nukeTable()

    @Query("select * from product where productName=:name")
    fun findProductByName(name: String): List<Product>

    @Query("select * from product where productID= :productID")
    fun findProduct(productID: Long): Product
}