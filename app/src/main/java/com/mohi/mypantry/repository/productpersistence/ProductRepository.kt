package com.mohi.mypantry.repository.productpersistence

import android.util.Log
import com.mohi.mypantry.LOG_TAG
import com.mohi.mypantry.domain.Product
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/24/2021
 */
class ProductRepository(private val productDao: ProductDao): IProductRepository {
    companion object{
        fun getInstance(productDao: ProductDao) = ProductRepository(productDao)
    }

    override fun getAllProducts(query: String?): Flow<List<Product>> {
        var finalQuery = query
        if(finalQuery.isNullOrEmpty())
            finalQuery = "%"
        else
            finalQuery = "%$query%"
        return productDao.getAllProducts(finalQuery)
    }

    override fun save(product: Product) {
        Log.d(LOG_TAG, "saving product $product")
        productDao.saveProduct(product)
    }

    override fun nuke() {
        productDao.nukeTable()
    }

    override fun update(product: Product) {
        productDao.updateProduct(product)
    }
}