package com.mohi.mypantry.repository.recipepersistence

import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/28/2021
 */
interface IRecipeRepository {
    /**
     * Get all the recipes from the database
     * @return a flow containing a list of recipes
     */
    fun getAllRecipes(query: String?): Flow<List<Recipe>>

    /**
     * Save a recipe to the database
     * @param recipe the recipe to be saved
     */
    fun save(recipe: Recipe)

    /**
     * Get the recipe and all its ingredients
     * @param recipeID the id of the recipe
     */
    fun getRecipeWithProducts(recipeID: Long): RecipeWithProducts

    /**
     * Save a recipe and all its ingredients
     * @param recipeWithProducts the recipe with the corresponding ingredients
     */
    fun saveRecipeWithProducts(recipeWithProducts: RecipeWithProducts)

    /**
     * Update a recipe and all its ingredients
     * @param recipeWithProducts the recipe with the corresponding ingredients
     */
    fun updateRecipeWithProducts(recipeWithProducts: RecipeWithProducts)
}