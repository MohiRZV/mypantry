package com.mohi.mypantry.repository.recipepersistence

import android.util.Log
import androidx.room.*
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeProduct
import com.mohi.mypantry.domain.RecipeWithProducts
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/28/2021
 */
//TODO add index on recipeID
// warning: The column recipeID in the junction entity com.mohi.mypantry.domain.RecipeProduct is being used to resolve a relationship but it is not covered by any index.
@Dao
interface RecipeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveRecipe(recipe: Recipe): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveProduct(product: Product): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveRecipeProduct(recipeProduct: RecipeProduct)

    fun saveRecipeWithProducts(recipeWithProducts: RecipeWithProducts): Long{
        val recipeID = saveRecipe(recipeWithProducts.recipe)
        for(product in recipeWithProducts.products){
            val newProduct = product.copy(quantity = 0.0)
            val productID = saveProduct(newProduct)
            saveRecipeProduct(RecipeProduct(productID, recipeID, product.quantity))
        }
        return recipeID
    }

    @Query("select * from recipe where recipeID= :recipeID")
    fun getRecipe(recipeID: Long): Recipe


    @Query("select * from product P inner join recipe_product RP on P.productID=RP.productID where recipeID= :recipeID")
    @RewriteQueriesToDropUnusedColumns
    fun getIngredients(recipeID: Long): List<Product>

    fun getRecipeWithProducts(recipeID: Long): RecipeWithProducts {
        val recipe = getRecipe(recipeID)
        val products = getIngredients(recipeID)

        return RecipeWithProducts(recipe, products)
    }

    @Transaction
    @RewriteQueriesToDropUnusedColumns
    @Query("select R.*, P.productID, P.productName, P.imageUriString, RP.quantity from recipe R inner join recipe_product RP on R.recipeID = RP.recipeID inner join product P on RP.productID = P.productID where P.productName = :product")
    fun findRecipesByProduct(product: String): List<RecipeWithProducts>

    @Update
    fun updateRecipe(recipe: Recipe)

    @Query("update recipe_product set quantity=:quantity where recipeID=:recipeID and productID=:productID")
    fun updateProductQuantity(productID: Long, recipeID: Long, quantity: Double)

    // helper method that returns a map of <productID, Product> for the given products list
    fun getIngredientsMapForRecipe(products: List<Product>): Map<Long, Product>{
        val ingredientsMap = mutableMapOf<Long, Product>()

        products.forEach {
            ingredientsMap[it.productID] = it
        }

        return ingredientsMap
    }

    fun updateRecipeWithProducts(recipeWithProducts: RecipeWithProducts){
        // update the recipe's attributes
        updateRecipe(recipeWithProducts.recipe)

        // obtain maps for the ingredients for faster searching
        val oldIngredientsMap = getIngredientsMapForRecipe(getRecipeWithProducts(recipeWithProducts.recipe.recipeID).products)
        val ingredientsMap = getIngredientsMapForRecipe(recipeWithProducts.products)

        // if the ingredient was removed from the recipe, delete the corresponding association
        oldIngredientsMap.forEach{
            if(!ingredientsMap.containsKey(it.key)) {
                deleteRecipeProduct(recipeWithProducts.recipe.recipeID, it.value.productID)
            }
        }

        recipeWithProducts.products.forEach {
            if(it.productID == 0L){//if the product is not in the database
                val newProduct = it.copy(quantity = 0.0)
                val newProductID = saveProduct(newProduct)
                saveRecipeProduct(RecipeProduct(newProductID, recipeWithProducts.recipe.recipeID, it.quantity))
            }else {
                updateProductQuantity(it.productID, recipeWithProducts.recipe.recipeID, it.quantity)
            }
        }
    }

    @Query("delete from recipe_product where recipeID=:recipeID and productID=:productID")
    fun deleteRecipeProduct(recipeID: Long, productID: Long)

    @Query("SELECT * FROM recipe where recipeName like :query")
    fun getAll(query: String): Flow<List<Recipe>>

    @Query("DELETE FROM recipe")
    fun nukeTable()
}