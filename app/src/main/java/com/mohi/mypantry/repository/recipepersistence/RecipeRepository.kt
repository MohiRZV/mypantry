package com.mohi.mypantry.repository.recipepersistence

import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 *   created by Mohi on 8/28/2021
 */
class RecipeRepository(private val recipeDao: RecipeDao): IRecipeRepository {
    companion object{
        fun getInstance(recipeDao: RecipeDao) = RecipeRepository(recipeDao)
    }

    override fun getAllRecipes(query: String?): Flow<List<Recipe>> {
        val finalQuery: String = if(query==null)
            "%"
        else
            "%$query%"
        return recipeDao.getAll(finalQuery)
    }

    override fun save(recipe: Recipe) {
        recipeDao.saveRecipe(recipe)
    }

    override fun getRecipeWithProducts(recipeID: Long): RecipeWithProducts {
        return recipeDao.getRecipeWithProducts(recipeID)
    }

    override fun saveRecipeWithProducts(recipeWithProducts: RecipeWithProducts) {
        recipeDao.saveRecipeWithProducts(recipeWithProducts)
    }

    override fun updateRecipeWithProducts(recipeWithProducts: RecipeWithProducts) {
        recipeDao.updateRecipeWithProducts(recipeWithProducts)
    }
}