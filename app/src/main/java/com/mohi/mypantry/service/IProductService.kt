package com.mohi.mypantry.service

import com.mohi.mypantry.domain.Product
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/24/2021
 */
interface IProductService {
    /**
     * Get all the products from the database
     * @return a flow containing a list of products
     */
    fun getAllProducts(query: String?): Flow<List<Product>>

    /**
     * Save a product in the database
     * @param product the product to be saved
     */
    fun saveProduct(product: Product)

    /**
     * Update the name and/or quantity of the product
     * @param product the product to be updated
     */
    fun updateProduct(product: Product)
}