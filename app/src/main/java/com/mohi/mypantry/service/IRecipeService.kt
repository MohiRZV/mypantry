package com.mohi.mypantry.service

import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/30/2021
 */
interface IRecipeService {
    /**
     * Get all the recipes from the database
     * @return a flow containing a list of recipes
     */
    fun getAllRecipes(query: String?): Flow<List<Recipe>>

    /**
     * Save a recipe to the database
     * @param recipe the recipe to be saved
     */
    fun saveRecipe(recipe: Recipe)

    /**
     * Get the recipe and all its ingredients
     * @param recipe the recipe for which the ingredients are needed
     */
    fun getRecipeWithProducts(recipe: Recipe): RecipeWithProducts

    /**
     * Save a recipe and all its ingredients
     * @param recipeWithProducts the recipe with the corresponding ingredients
     */
    fun saveRecipeWithProducts(recipeWithProducts: RecipeWithProducts)

    /**
     * Update a recipe and all its ingredients
     * @param recipeWithProducts the recipe with the corresponding ingredients
     */
    fun updateRecipeWithProducts(recipeWithProducts: RecipeWithProducts)
}