package com.mohi.mypantry.service

import com.mohi.mypantry.MainActivity
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.repository.PantryDatabase
import com.mohi.mypantry.repository.productpersistence.IProductRepository
import com.mohi.mypantry.repository.productpersistence.ProductRepository
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/24/2021
 */
class ProductService: IProductService {

    companion object{
        private val productService = ProductService()
        fun getInstance() = productService
    }

    private val productRepo: IProductRepository = ProductRepository.getInstance(PantryDatabase.getDatabase(MainActivity.instance.applicationContext).productDao())

    override fun getAllProducts(query: String?): Flow<List<Product>> {
        return productRepo.getAllProducts(query)
    }

    override fun saveProduct(product: Product) {
        productRepo.save(product)
    }

    override fun updateProduct(product: Product) {
        productRepo.update(product)
    }
}