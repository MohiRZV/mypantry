package com.mohi.mypantry.service

import com.mohi.mypantry.MainActivity
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import com.mohi.mypantry.repository.PantryDatabase
import com.mohi.mypantry.repository.recipepersistence.IRecipeRepository
import com.mohi.mypantry.repository.recipepersistence.RecipeRepository
import kotlinx.coroutines.flow.Flow

/**
 *   created by Mohi on 8/30/2021
 */
class RecipeService: IRecipeService {

    private val recipeRepo: IRecipeRepository =
        RecipeRepository.getInstance(PantryDatabase.getDatabase(MainActivity.instance.applicationContext).recipeDao())

    override fun getAllRecipes(query: String?): Flow<List<Recipe>> {
        return recipeRepo.getAllRecipes(query)
    }

    override fun saveRecipe(recipe: Recipe) {
        recipeRepo.save(recipe)
    }

    override fun getRecipeWithProducts(recipe: Recipe): RecipeWithProducts {
        return recipeRepo.getRecipeWithProducts(recipe.recipeID)
    }

    override fun saveRecipeWithProducts(recipeWithProducts: RecipeWithProducts) {
        recipeRepo.saveRecipeWithProducts(recipeWithProducts)
    }

    override fun updateRecipeWithProducts(recipeWithProducts: RecipeWithProducts) {
        recipeRepo.updateRecipeWithProducts(recipeWithProducts)
    }
}