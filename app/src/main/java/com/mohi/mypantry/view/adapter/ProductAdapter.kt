package com.mohi.mypantry.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mohi.mypantry.R
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.ProductDiffCallback

/**
 *   created by Mohi on 8/24/2021
 */
interface OnProductClickInterface{
    /**
     * Click event for when a product from the recyclerView is pressed
     * @param item the product that was pressed
     */
    fun onClick(item: Product)
}

class ProductAdapter(private val clickListener: OnProductClickInterface):
    ListAdapter<Product, ProductViewHolder>(ProductDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.name.text = currentList[position].name
        holder.quantity.text = currentList[position].quantity.toString()
//        holder.productImage
        holder.container.setOnClickListener {
            clickListener.onClick(currentList[position])
        }
    }

    /**
     * Add a product to the recyclerView
     * @param product the product to be added
     */
    fun add(product: Product){
        // create a copy of the currentList
        val list = ArrayList(currentList)

        // add the product to the copy
        list.add(product)

        // update the list from the adapter
        submitList(list)
    }

    /**
     * Update a product from the recyclerView
     * @param product the product to be updated
     */
    fun updateProduct(product: Product){
        // search for the product in the list
        for(position in 0 until currentList.size){
            val it = currentList[position]
            if(it.productID == product.productID){ // when found
                // update its name and quantity
                it.name = product.name
                it.quantity = product.quantity
                // notify the observer with the position of the product
                notifyItemChanged(position)
                // there shouldn't be more than one product with the same id in the list
                break
            }
        }
    }
}

class ProductViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    var container: ViewGroup = itemView.findViewById(R.id.product_item_container)
    var receiptsContaining: TextView = itemView.findViewById(R.id.receipts_containing)
    var productImage: ImageView = itemView.findViewById(R.id.product_image)
    var name: TextView = itemView.findViewById(R.id.product_name)
    var quantity: TextView = itemView.findViewById(R.id.product_quantity)

}