package com.mohi.mypantry.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mohi.mypantry.R
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeDiffCallback

/**
 *   created by Mohi on 8/30/2021
 */
interface OnRecipeClickInterface{
    /**
     * Click event for when a recipe from the recyclerView is pressed
     * @param item the recipe that was pressed
     */
    fun onClick(item: Recipe)
}

class RecipeAdapter(private val clickListener: OnRecipeClickInterface):
    ListAdapter<Recipe, RecipeViewHolder>(RecipeDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.recipe_item, parent, false)
        return RecipeViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        holder.name.text = currentList[position].name
//        holder.recipeImage
        holder.container.setOnClickListener {
            clickListener.onClick(currentList[position])
        }
    }
}

class RecipeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    var container: ViewGroup = itemView.findViewById(R.id.recipe_item_container)
    var recipeImage: ImageView = itemView.findViewById(R.id.recipe_image)
    var name: TextView = itemView.findViewById(R.id.recipe_name)
}