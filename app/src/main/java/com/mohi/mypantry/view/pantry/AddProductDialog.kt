package com.mohi.mypantry.view.pantry

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText
import com.mohi.mypantry.R
import com.mohi.mypantry.domain.Product
import java.lang.NumberFormatException

/**
 *   created by Mohi on 8/28/2021
 */
interface OnAddClickListener{
    /**
     * Click event for when the add button from the product dialog is pressed
     * @param name the name of the product
     * @param quantity the quantity of the product
     */
    fun onAdd(name: String, quantity: Double)
}

class AddProductDialog(private val givenContext: Context, private val onAddClickListener: OnAddClickListener): Dialog(givenContext) {

    private lateinit var buttonAdd: Button
    private lateinit var buttonCancel: Button
    private lateinit var name: TextInputEditText
    private lateinit var quantity: TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.add_product_dialog_layout)

        buttonAdd = findViewById(R.id.button_add)
        buttonCancel = findViewById(R.id.button_cancel)
        name = findViewById(R.id.text_input_name)
        quantity = findViewById(R.id.text_input_quantity)

        buttonAdd.setOnClickListener {
            addProduct()
        }

        buttonCancel.setOnClickListener {
            cancel()
        }
    }

    /**
     * Add button click listener
     */
    private fun addProduct() {
        // parse the attributes of the product from the text boxes
        val productName = name.text.toString()
        val productQuantity = quantity.text.toString()
        val quantityDouble: Double
        try {
            quantityDouble = productQuantity.toDouble()
        }catch (ex: NumberFormatException){
            Toast.makeText(givenContext, "Quantity must be a number!", Toast.LENGTH_SHORT).show()
            return
        }

        // call the listener for adding the product
        onAddClickListener.onAdd(productName, quantityDouble)

        dismiss()
    }

    override fun onStop() {
        super.onStop()
        // clear the text boxes
        name.setText(R.string.empty)
        quantity.setText(R.string.empty)
    }

    /**
     * Open the dialog in the edit mode
     */
    fun editProduct(product: Product){
        // set the button name to Update
        buttonAdd.text = givenContext.getString(R.string.update_button_text)
        fillData(product)
    }

    /**
     * Fill the text boxes with the product information
     * @param item the product whose data will be filled
     */
    private fun fillData(item: Product) {
        name.setText(item.name)
        quantity.setText(item.quantity.toString())
    }
}