package com.mohi.mypantry.view.pantry

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mohi.mypantry.R
import com.mohi.mypantry.databinding.FragmentHomeBinding
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.view.adapter.OnProductClickInterface
import com.mohi.mypantry.view.adapter.ProductAdapter
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import com.mohi.mypantry.MainActivity

import android.view.MenuItem

import android.util.Log
import com.mohi.mypantry.LOG_TAG
import androidx.core.content.ContextCompat.getSystemService

import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import com.mohi.mypantry.view.utils.hideKeyboard


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    private lateinit var addDialog : AddProductDialog
    private lateinit var adapter: ProductAdapter
    private lateinit var searchView: SearchView

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        requireActivity().title = "My pantry"
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        setHasOptionsMenu(true)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialize()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        searchView = SearchView((context as MainActivity).supportActionBar?.themedContext ?: context)
        val menuItem = menu.findItem(R.id.action_search).apply {
            setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
            actionView = searchView
        }
        initializeSearch()
        menuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            // when the back icon is pressed, show all the products
            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                searchView.setQuery("", false)
                lifecycle.coroutineScope.launch {
                    homeViewModel.getAllProducts().collect {
                        adapter.submitList(it)
                    }
                }
                return true
            }

        })
    }

    private fun initialize() {
        setRecyclerView()

        val addListener = object : OnAddClickListener{
            override fun onAdd(name: String, quantity: Double) {
                homeViewModel.saveProduct(Product(name, quantity))
            }
        }

        addDialog = AddProductDialog(requireContext(), addListener)
        addDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.fab.setOnClickListener {
            addDialog.show()
        }


    }

    private fun initializeSearch(){
        Log.d(LOG_TAG, "init search view")
        searchView.isIconifiedByDefault = false
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            // the products are filtered on onQueryTextChange
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d(LOG_TAG, "onQueryTextSubmit $query")
                return true
            }

            // update the products shown each time a letter in typed
            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    lifecycle.coroutineScope.launch {
                        homeViewModel.getAllProducts(it).collect {
                            adapter.submitList(it)
                        }
                    }
                }
                return true
            }
        })

        // when the search is closed, display all the products again
        searchView.setOnCloseListener {
            lifecycle.coroutineScope.launch {
                homeViewModel.getAllProducts().collect {
                    adapter.submitList(it)
                }
            }
            hideKeyboard()
            true
        }


    }

    private fun setRecyclerView(){
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        val clickListener: OnProductClickInterface = object: OnProductClickInterface{
            override fun onClick(item: Product) {

                val onUpdateClickListener = object : OnAddClickListener{
                    override fun onAdd(name: String, quantity: Double) {
                        homeViewModel.updateProduct(Product(name, quantity, productID = item.productID))
                    }

                }

                val dialog = AddProductDialog(requireContext(), onUpdateClickListener)

                dialog.show()

                dialog.editProduct(item)
            }
        }

        adapter = ProductAdapter(clickListener)
        binding.recyclerViewProducts.adapter = adapter

        lifecycle.coroutineScope.launch {
            homeViewModel.getAllProducts().collect {
                adapter.submitList(it)
            }
        }

        binding.recyclerViewProducts.layoutManager = layoutManager
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}