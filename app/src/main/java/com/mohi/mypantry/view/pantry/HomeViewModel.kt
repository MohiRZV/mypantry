package com.mohi.mypantry.view.pantry

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.service.ProductService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val productService = ProductService.getInstance()

    /**
     * Get all the products from the database
     * @return a flow containing a list of products
     */
    fun getAllProducts(query: String? = null): Flow<List<Product>> {
        return productService.getAllProducts(query)
    }

    /**
     * Save a product in the database
     * @param product the product to be saved
     */
    fun saveProduct(product: Product) {
        viewModelScope.launch(Dispatchers.IO) {
            productService.saveProduct(product)
        }
    }

    /**
     * Update the name and/or quantity of the product
     * @param product the product to be updated
     */
    fun updateProduct(product: Product) {
        viewModelScope.launch(Dispatchers.IO) {
            productService.updateProduct(product)
        }
    }

}