package com.mohi.mypantry.view.recipes

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.mohi.mypantry.LOG_TAG
import com.mohi.mypantry.R
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import com.mohi.mypantry.view.adapter.OnProductClickInterface
import com.mohi.mypantry.view.adapter.ProductAdapter
import com.mohi.mypantry.view.pantry.AddProductDialog
import com.mohi.mypantry.view.pantry.OnAddClickListener

/**
 *   created by Mohi on 8/30/2021
 */
interface OnAddRecipeClickListener{
    /**
     * Click event for when the add button from the recipe dialog is pressed
     * @param recipe the recipe
     * @param products a list of products
     */
    fun onAdd(recipe: Recipe, products: List<Product>)
}

class AddRecipeDialog(private val givenContext: Context, private val onAddRecipeClickListener: OnAddRecipeClickListener): Dialog(givenContext) {
    private lateinit var buttonAdd: Button
    private lateinit var buttonAddIngredient: Button
    private lateinit var buttonCancel: Button
    private lateinit var name: TextInputEditText
    private lateinit var description: TextInputEditText
    private lateinit var recyclerViewIngredients: RecyclerView
    private lateinit var adapter: ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.add_recipe_dialog_layout)

        buttonAdd = findViewById(R.id.button_add)
        buttonAddIngredient = findViewById(R.id.button_add_ingredient)
        buttonCancel = findViewById(R.id.button_cancel)
        name = findViewById(R.id.text_input_name)
        description = findViewById(R.id.text_input_description)
        recyclerViewIngredients = findViewById(R.id.recycler_view_ingredients)

        setRecyclerView()

        buttonAddIngredient.setOnClickListener {
            displayAddIngredientDialog()
        }

        buttonAdd.setOnClickListener {
            addRecipe()
        }

        buttonCancel.setOnClickListener {
            cancel()
        }
    }

    /**
     * Fill the text boxes with the recipe information and the list of ingredients
     * @param recipeWithProducts an object containing the recipe and its ingredients
     */
    private fun fillData(recipeWithProducts: RecipeWithProducts){
        name.setText(recipeWithProducts.recipe.name)
        description.setText(recipeWithProducts.recipe.description)
        adapter.submitList(recipeWithProducts.products)
    }

    /**
     * Open the dialog in the edit mode
     */
    fun editRecipe(recipeWithProducts: RecipeWithProducts){
        buttonAdd.text = givenContext.getString(R.string.update_button_text)
        fillData(recipeWithProducts)
    }

    /**
     * Display the add product dialog
     * @return an AddProductDialog whose add button adds the product to the recyclerView
     */
    private fun displayAddIngredientDialog(): AddProductDialog {

        val addListener = object: OnAddClickListener{
            override fun onAdd(name: String, quantity: Double) {
               adapter.add(Product(name, quantity))
            }
        }

        val dialog = AddProductDialog(context, addListener)

        dialog.show()

        return dialog
    }

    /**
     * Initialize the recyclerView
     */
    private fun setRecyclerView(){
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        val clickListener: OnProductClickInterface = object: OnProductClickInterface {
            override fun onClick(item: Product) {

                val onAddClickListener = object: OnAddClickListener{
                    override fun onAdd(name: String, quantity: Double) {
                        adapter.updateProduct(Product(name, quantity, productID = item.productID))
                    }
                }

                val dialog = AddProductDialog(context, onAddClickListener)

                dialog.show()

                dialog.editProduct(item)

            }
        }

        adapter = ProductAdapter(clickListener)
        recyclerViewIngredients.adapter = adapter

        recyclerViewIngredients.layoutManager = layoutManager
    }


    private fun addRecipe() {
        // parse the recipe fields
        val recipeName = name.text.toString()
        val recipeDescription = description.text.toString()

        // call the listener for adding a recipe with products
        onAddRecipeClickListener.onAdd(Recipe(recipeName, recipeDescription), adapter.currentList)

        Log.d(LOG_TAG, "$recipeName, $recipeDescription, ${adapter.currentList}")

        dismiss()
    }

    override fun onStop() {
        super.onStop()
        // clear the text boxes
        name.setText(R.string.empty)
        description.setText(R.string.empty)
    }
}