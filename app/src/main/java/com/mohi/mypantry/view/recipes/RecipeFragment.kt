package com.mohi.mypantry.view.recipes

import android.app.SearchManager
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.coroutineScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mohi.mypantry.LOG_TAG
import com.mohi.mypantry.MainActivity
import com.mohi.mypantry.R
import com.mohi.mypantry.databinding.FragmentRecipeBinding
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import com.mohi.mypantry.view.adapter.OnRecipeClickInterface
import com.mohi.mypantry.view.adapter.RecipeAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import androidx.core.content.ContextCompat.getSystemService

import android.view.inputmethod.InputMethodManager

import android.view.View
import com.mohi.mypantry.view.utils.hideKeyboard

class RecipeFragment : Fragment() {

    private lateinit var recipeViewModel: RecipeViewModel
    private var _binding: FragmentRecipeBinding? = null
    private lateinit var searchView: SearchView
    private lateinit var adapter: RecipeAdapter

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        recipeViewModel =
            ViewModelProvider(this).get(RecipeViewModel::class.java)

        _binding = FragmentRecipeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        setHasOptionsMenu(true)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialize()
    }

    private fun initialize() {
        setRecyclerView()

        val addListener = object : OnAddRecipeClickListener {
            override fun onAdd(recipe: Recipe, products: List<Product>) {
                recipeViewModel.saveRecipeWithProducts(recipe, products)
            }
        }

        val addDialog = AddRecipeDialog(requireContext(), addListener)
        addDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.fab.setOnClickListener {
            addDialog.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        searchView = SearchView((context as MainActivity).supportActionBar?.themedContext ?: context)
        val menuItem = menu.findItem(R.id.action_search).apply {
            setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW or MenuItem.SHOW_AS_ACTION_IF_ROOM)
            actionView = searchView
        }
        initializeSearch()
        menuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener{
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            // when the back icon is pressed, show all the products
            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                searchView.setQuery("", false)
                lifecycle.coroutineScope.launch {
                    recipeViewModel.getAllRecipes().collect {
                        adapter.submitList(it)
                    }
                    hideKeyboard()
                }
                return true
            }

        })
    }

    private fun initializeSearch(){
        Log.d(LOG_TAG, "RecipeFragment initializeSearch")
        searchView.isIconifiedByDefault = false
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            // the recipes are filtered on onQueryTextChange
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d(LOG_TAG, "RecipeFragment onQueryTextSubmit $query")
                return true
            }

            // update the recipes shown each time a letter in typed
            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    lifecycle.coroutineScope.launch {
                        recipeViewModel.getAllRecipes(it).collect {
                            adapter.submitList(it)
                        }
                    }
                }
                return true
            }
        })

        // when the search is closed, display all the recipes again
        searchView.setOnCloseListener {
            lifecycle.coroutineScope.launch {
                recipeViewModel.getAllRecipes().collect {
                    adapter.submitList(it)
                }
            }
            true
        }


    }

    //TODO open edit product dialog
    //TODO update recipe and products
    //TODO restrict user from adding a product that already exists via the recipe interface

    private fun setRecyclerView(){
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        val clickListener: OnRecipeClickInterface = object: OnRecipeClickInterface {
            override fun onClick(item: Recipe) {
                Toast.makeText(context, "Recipe $item clicked", Toast.LENGTH_SHORT).show()
                val onAddRecipeClickListener = object : OnAddRecipeClickListener{
                    override fun onAdd(recipe: Recipe, products: List<Product>) {
                        // set the id of the current edited recipe
                        recipe.recipeID = item.recipeID
                        recipeViewModel.updateRecipeWithProducts(RecipeWithProducts(recipe, products))
                    }
                }

                val dialog =  AddRecipeDialog(requireContext(), onAddRecipeClickListener)

                dialog.show()

                lifecycle.coroutineScope.launch(Dispatchers.IO) {
                    // obtain the RecipeWithProducts object from the database
                    val recipeWithProducts = recipeViewModel.getRecipeWithProducts(item)

                    // post the results inside the dialog
                    withContext(Dispatchers.Main) {
                        dialog.editRecipe(recipeWithProducts)
                    }
                }

            }
        }

        adapter = RecipeAdapter(clickListener)
        binding.recyclerViewRecipes.adapter = adapter

        lifecycle.coroutineScope.launch {
            recipeViewModel.getAllRecipes().collect {
                adapter.submitList(it)
            }
        }

        binding.recyclerViewRecipes.layoutManager = layoutManager
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}