package com.mohi.mypantry.view.recipes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mohi.mypantry.domain.Product
import com.mohi.mypantry.domain.Recipe
import com.mohi.mypantry.domain.RecipeWithProducts
import com.mohi.mypantry.service.IRecipeService
import com.mohi.mypantry.service.RecipeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class RecipeViewModel : ViewModel() {


    private val recipeService: IRecipeService = RecipeService()

    fun getAllRecipes(query: String? = null): Flow<List<Recipe>>{
        return recipeService.getAllRecipes(query)
    }

    fun saveRecipe(recipe: Recipe) {
        viewModelScope.launch(Dispatchers.IO) {
            recipeService.saveRecipe(recipe)
        }
    }

    fun getRecipeWithProducts(item: Recipe): RecipeWithProducts {
        return recipeService.getRecipeWithProducts(item)
    }

    fun saveRecipeWithProducts(recipe: Recipe, products: List<Product>) {
        viewModelScope.launch(Dispatchers.IO) {
            recipeService.saveRecipeWithProducts(RecipeWithProducts(recipe, products))
        }
    }

    fun updateRecipeWithProducts(recipeWithProducts: RecipeWithProducts) {
        viewModelScope.launch(Dispatchers.IO) {
            recipeService.updateRecipeWithProducts(recipeWithProducts)
        }
    }
}